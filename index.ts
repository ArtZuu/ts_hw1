const canvas = document.getElementById('canvas') as HTMLCanvasElement;
const ctx = canvas.getContext('2d');

ctx.fillStyle = "#f79554";
ctx.fillRect(0, 0, canvas.width, canvas.height);

let xPoints:number[] = [];
let yPoints:number[] = [];

const addPointBtn = document.getElementById('addPoint') as HTMLButtonElement;
const drawPointBtn = document.getElementById('drawPoint') as HTMLButtonElement;
const gerResultBtn = document.getElementById('getResult') as HTMLButtonElement;
const perimeter = document.getElementById('perimeter');
const area = document.getElementById('area');

addPointBtn.addEventListener('click', () => {
  addPoint();
});

drawPointBtn.addEventListener('click', () => {
  drawPoint();
});

gerResultBtn.addEventListener('click', () => {
  let perimeterResult = getPerimeter(xPoints, yPoints)+'';
  let areaResult = getArea(xPoints, yPoints)+'';
  perimeter.innerHTML = perimeterResult;
  area.innerHTML = areaResult;
});

function drawPoint(): void {
  ctx.beginPath();
  ctx.moveTo(xPoints[0], yPoints[0]);
  let restX = xPoints.slice(1);
  let restY = yPoints.slice(1);

  restX.forEach((x, index) => {
    let y = restY[index];
    ctx.lineTo(x, y);
    ctx.stroke();
  });
};

function addPoint(): void {
  let startX = +((<HTMLInputElement>document.getElementById('startX')).value);
  let startY = +((<HTMLInputElement>document.getElementById('startY')).value);
  xPoints.push(startX);
  yPoints.push(startY);
}

function getPerimeter(xDots:number[], yDots:number[]):number {
  let perimeter = 0;
  for (let i = 0; i < xDots.length - 1; i++) {
    for (let j = 0; j < yDots.length - 1; j++) {
      let side = Math.sqrt(((xDots[i+1]-xDots[i])**2) + ((yDots[j+1]-yDots[j])**2));
      perimeter += side;
    };
  };
  return perimeter;
};

function getArea(xDots:number[], yDots:number[]):number {
  let area = 0;
  for (let i = 0; i < xDots.length - 1; i++) {
    for (let j = 0; j < yDots.length - 1; j++) {
      area += xDots[i-1] * yDots[j] - yDots[j-1]*xDots[i];
    };
  };
  area = Math.abs(area/2);
  return area;
};