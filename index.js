var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
ctx.fillStyle = "#f79554";
ctx.fillRect(0, 0, canvas.width, canvas.height);
var xPoints = [];
var yPoints = [];
var addPointBtn = document.getElementById('addPoint');
var drawPointBtn = document.getElementById('drawPoint');
var gerResultBtn = document.getElementById('getResult');
var perimeter = document.getElementById('perimeter');
var area = document.getElementById('area');
addPointBtn.addEventListener('click', function () {
    addPoint();
});
drawPointBtn.addEventListener('click', function () {
    drawPoint();
});
gerResultBtn.addEventListener('click', function () {
    var perimeterResult = getPerimeter(xPoints, yPoints) + '';
    var areaResult = getArea(xPoints, yPoints) + '';
    perimeter.innerHTML = perimeterResult;
    area.innerHTML = areaResult;
});
function drawPoint() {
    ctx.beginPath();
    ctx.moveTo(xPoints[0], yPoints[0]);
    var restX = xPoints.slice(1);
    var restY = yPoints.slice(1);
    restX.forEach(function (x, index) {
        var y = restY[index];
        ctx.lineTo(x, y);
        ctx.stroke();
    });
}
;
function addPoint() {
    var startX = +(document.getElementById('startX').value);
    var startY = +(document.getElementById('startY').value);
    xPoints.push(startX);
    yPoints.push(startY);
}
function getPerimeter(xDots, yDots) {
    var perimeter = 0;
    for (var i = 0; i < xDots.length - 1; i++) {
        for (var j = 0; j < yDots.length - 1; j++) {
            var side = Math.sqrt((Math.pow((xDots[i + 1] - xDots[i]), 2)) + (Math.pow((yDots[j + 1] - yDots[j]), 2)));
            perimeter += side;
        }
        ;
    }
    ;
    return perimeter;
}
;
function getArea(xDots, yDots) {
    var area = 0;
    for (var i = 0; i < xDots.length - 1; i++) {
        for (var j = 0; j < yDots.length - 1; j++) {
            area += xDots[i - 1] * yPoints[j] - yPoints[j - 1] * xPoints[i];
        }
        ;
    }
    ;
    area = Math.abs(area / 2);
    return area;
}
;
